from django.apps import AppConfig


class ProjetTestConfig(AppConfig):
    name = 'projet_test'
