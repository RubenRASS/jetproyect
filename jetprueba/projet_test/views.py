from django.shortcuts import render
from django.http import HttpResponse
from .models import persona
# Create your views here.
def index(request):
    pers = persona.objects.all()

    return render(request, 'view_client/index.html', {'pers': pers,})
