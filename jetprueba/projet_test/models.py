from django.db import models

# Create your models here.
class persona(models.Model):
    nombre = models.CharField(max_length=300)
    edad = models.IntegerField(null=False)
    rut = models.CharField(max_length=45)
    carrera = models.CharField(max_length=200)
    institucion = models.CharField(max_length=300)
    horaspractica = models.IntegerField(null=False)
    # def __init__(self, arg):
    #     super(prueba, self).__init__()
    #     self.arg = arg
    def __str__(self):
        return self.nombre
